var path = require('path');

module.exports = {
  entry: path.join(__dirname, 'src', 'calendar.js'),
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'calendar.js',
    library: 'Calendar',
    libraryTarget: 'commonjs2'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel'
      }
    ]
  },
  target: "node"
};
