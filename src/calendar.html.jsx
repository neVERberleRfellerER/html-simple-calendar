/** @jsx dom */

const attributeMapping = {
    "className": "class"
};

function domAppendChildren(el, children) {
    for (let i = 0, n = children.length; i < n; ++i) {
        const child = children[i];
        if (Array.isArray(child)) {
            domAppendChildren(el, child);
        } else if (child.nodeName) {
            el.appendChild(child);
        } else {
            el.appendChild(document.createTextNode('' + child));
        }
    }
}

function dom(tag, attrs, ...children) {
    const elem = document.createElement(tag);
    for (const attr in attrs) {
        const mappedAttr = attributeMapping[attr] || attr;
        elem.setAttribute(mappedAttr, attrs[attr]);
    }
    domAppendChildren(elem, children);
    return elem;
}

function renderMonthRows(ctx) {
    const rows = [];
    for (let row = 0; row < 6; ++row) {
        const cols = [];
        for (let col = 0; col < 7; ++col) {
            const n = 1 + (col - ctx.firstDay + 1) + (row - 1) * 7;
            if (n >= 1 && n <= ctx.dayCount) {
                cols.push(
                    <td data-day={n}
                        className={n === ctx.date.day ? 'active' : ''}>
                        {n}
                    </td>
                );
            } else {
                cols.push(<td className="empty"> </td>);
            }
        }
        rows.push(<tr>{cols}</tr>);
    }
    return rows;
}

export default function (ctx) {
    return (
        <table>
            <thead>
                <tr>
                    <td colspan={ctx.locale.days.length}>
                        <span className="prev">&lt;</span>
                        <span>
                            <span className="month">
                                {ctx.locale.months[ctx.date.month]}
                            </span>
                            {' '}
                            <span className="year">
                                {ctx.date.year}
                            </span>
                        </span>
                        <span className="next">&gt;</span>
                    </td>
                </tr>
                <tr className="day-row">
                    {ctx.locale.days.map(function (dayName) {
                        return <td>{dayName}</td>;
                    })}
                </tr>
            </thead>
            <tbody>
                {renderMonthRows(ctx)}
            </tbody>
        </table>
    );
}
