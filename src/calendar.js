import { EventEmitter } from 'events';
import tableTemplate from './calendar.html.jsx';

function daysInMonth(year, month) {
  return new Date(year, month, 0).getDate();
}

function getFirstDay(year, month) {
  return new Date(year, month - 1, 1).getDay();
}

function getTableIndicesForDate(date) {
  var tmp = new Date(date.getTime());
  tmp.setDate(1);

  var dateOffset = tmp.getDay();
  var offsetDate = date + (dateOffset - 1) - 1;

  return {
    row: Math.floor(offsetDate / 7),
    col: offsetDate % 7
  };
}

export default class Calendar {

  constructor(year, month, day) {
    this._emitter = new EventEmitter();

    var date = new Date();
    if (typeof year !== 'undefined') date.setYear(year);
    if (typeof month !== 'undefined') date.setMonth(month);
    if (typeof day !== 'undefined') date.setDate(day);
    this._date = date;
  }

  addEventListener(event, listener) {
    this._emitter.on(event, listener);
  }

  removeEventListener(event, listener) {
    this._emitter.removeListener(event, listener);
  }

  setYear(year) {
    var old = this._date.getFullYear();
    this._date.setFullYear(year);
    this._date.setDate(1);
    this._emitter.emit('yearChanged', {
      old,
      new: year
    });
  }

  setMonth(month) {
    var old = this._date.getMonth();
    this._date.setMonth(month);
    this._date.setDate(1);
    this._emitter.emit('monthChanged', {
      old,
      new: month
    });
  }

  setDay(day) {
    var old = this._date.getDay();
    this._date.setDate(day);
    this._emitter.emit('dayChanged', {
      old,
      new: day
    });
  }

  el() {
    return this._renderTable();
  }

  _renderTable() {
    var dummy = tableTemplate({
      locale: locales.cs,
      date: {
        day: this._date.getDate(),
        month: this._date.getMonth(),
        year: this._date.getFullYear()
      },
      firstDay: getFirstDay(this._date.getFullYear(), this._date.getMonth() + 1),
      dayCount: daysInMonth(this._date.getFullYear(), this._date.getMonth() + 1)
    });

    var prev = dummy.querySelector('.prev');
    var next = dummy.querySelector('.next');
    var tbody = dummy.querySelector('tbody');
    prev.addEventListener('click', this._onPrevMonth.bind(this));
    next.addEventListener('click', this._onNextMonth.bind(this));
    tbody.addEventListener('click', this._onDayClick.bind(this));

    return dummy;
  }

  _onDayClick(evt) {
    var day = evt.target.dataset.day;
    if (typeof day !== "string") {
      return;
    }

    var oldDate = new Date(this._date.getTime());
    day = parseInt(day, 10);
    this.setDay(day);

    this._emitter.emit('dayClicked', {
      oldDate: oldDate,
      newDate: new Date(this._date.getTime())
    });
  }

  _onPrevMonth() {
    this.setMonth(this._date.getMonth() - 1);
  }

  _onNextMonth() {
    this.setMonth(this._date.getMonth() + 1);
  }

}

export var locales = {
  "cs": {
    months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec",
             "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
    days: ["Po", "Út", "St", "Čt", "Pá", "So", "Ne"]
  }
};

//main for debug
/*
  var body = document.getElementById('body');
  var calendar = new Calendar();

  body.appendChild(calendar.el());
  function onChange(evt) {
      if (evt.newDate) {
          console.log(evt.newDate);
        }
      body.replaceChild(calendar.el(), body.firstChild);
    }

  calendar.addEventListener('dayChanged', onChange);
  calendar.addEventListener('monthChanged', onChange);
  calendar.addEventListener('yearChanged', onChange);
  calendar.addEventListener('dayClicked', onChange);
*/
